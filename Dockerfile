# victorlamoine/rosindustrial_vtk8:kinetic
FROM rosindustrial/core:kinetic
LABEL maintainer "victor.lamoine@gmail.com"

COPY setup_script.bash /
RUN bash -c "chmod +x /setup_script.bash && sync && /setup_script.bash"
RUN apt clean && \
    rm -rf /var/lib/apt/lists/*

