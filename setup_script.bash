#!/bin/bash

# Update ROS
apt update
apt full-upgrade -y

# Dependencies for the next steps
apt install -y wget unzip

# Download, compile, install latest VTK 8 release
wget http://www.vtk.org/files/release/8.0/VTK-8.0.1.zip
unzip VTK-8.0.1.zip
rm VTK-8.0.1.zip
mkdir /vtk_build
cd /vtk_build
cmake -DCMAKE_BUILD_TYPE=MinSizeRel ../VTK-8.0.1
make -j4
make -j4 install
ldconfig

# Clean
cd /
rm -rf /VTK-8.0.1 /vtk_build
